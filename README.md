# Encryption Compression File Transfer

* Name : Sulthan Mahendra Mubarak
* NRP : 4210181019


This program is used to encrypt and compress file and then the file will get sent to server, where it will be extracted and decrypted

1. Server will start a listener
2. Client will connect to Server
3. File path and name is assigned on the client, and program will prompt user to input password.
4. File will be encrypted with the password using aes encryption
5. File will be compressed to compression path, and then program will read the bytes of the zip
6. Client will send file size, zip name, and password to server.
7. Client will then send the file, where server will accept the file every 4 KB.
8. Server will extract the zip, and then decrypted the extracted file.